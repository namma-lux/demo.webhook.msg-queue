### Environment Configuration

1. Config database.
2. Set `QUEUE_CONNECTION=database`

### Run web server

`php artisan serve`

or

`php artisan --port=3000 --host=0.0.0.0`

### Run worker

`php artisan queue:work`

### Push all failed jobs back into the queue

`php artisan queue:retry all`

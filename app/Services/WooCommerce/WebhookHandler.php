<?php

namespace App\Services\WooCommerce;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;

class WebhookHandler
{
    public function getEventHandlerMap()
    {
        return [
            'order' => [
                'created' => 'handleOrderCreated',
                'updated' => 'handleOrderUpdated',
            ],
        ];
    }

    public function eventIsNotSupported(string $resource, string $event)
    {
        return (null === $this->getMethodNameToInvoke($resource, $event));
    }

    public function handle(string $source, string $resource, string $event, array $payload)
    {
        $methodNameToInvoke = $this->getMethodNameToInvoke($resource, $event);
        $data               = compact('source', 'resource', 'event', 'payload');

        $this->{$methodNameToInvoke}($data);
    }

    private function getMethodNameToInvoke(string $resource, string $event)
    {
        $map = $this->getEventHandlerMap();
        $key = "{$resource}.{$event}";

        return Arr::get($map, $key);
    }

    private function handleOrderCreated(array $data)
    {
        // TODO Insert data into DB.

        // Just for logging.
        $json = json_encode($data, JSON_PRETTY_PRINT);
        Log::info('Created new order with request: '.$json);
    }

    private function handleOrderUpdated(array $data)
    {
        // TODO Update data in DB.

        // Just for logging.
        $json = json_encode($data, JSON_PRETTY_PRINT);
        Log::info('Updated existing order with request: '.$json);
    }
}

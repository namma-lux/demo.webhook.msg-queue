<?php

namespace App\Http\Controllers\Webhooks;

use App\Http\Controllers\Controller;
use App\Jobs\HandleWooCommerceWebhookEvent;
use App\Services\WooCommerce\WebhookHandler;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class WooCommerceWebhookController extends Controller
{
    public function __invoke(Request $request, WebhookHandler $handler)
    {
        $source    = $request->headers->get('x-wc-webhook-source');
        $resource  = $request->headers->get('x-wc-webhook-resource');
        $event     = $request->headers->get('x-wc-webhook-event');
        $signature = $request->headers->get('x-wc-webhook-signature');

        // Authentication.
        if ($this->isNotAuthenticated($source, $signature))
            return \response('Invalid signature', 401);

        // Check if the event is supported.
        if ($handler->eventIsNotSupported($resource, $event))
            return \response("Event [{$event}] for resource [{$resource}] is not supported", 400);

        // Everything is valid -> push the event into message queue INSTEAD OF handling right now.
        try {
            $payload = $request->all();
            HandleWooCommerceWebhookEvent::dispatch($source, $resource, $event, $payload);

            return \response('OK');

        } catch (\Exception $e) {
            Log::error($e->getTraceAsString());
            return \response('Server error', 500);
        }
    }

    private function isNotAuthenticated(string $source, string $signature)
    {
        // TODO Check the signature for the source domain.
        //      If the signature is NOT valid, return [true].

        return false;
    }
}

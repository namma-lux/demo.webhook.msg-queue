<?php

namespace App\Jobs;

use App\Services\WooCommerce\WebhookHandler;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class HandleWooCommerceWebhookEvent implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var string */
    private $source;

    /** @var string */
    private $resource;

    /** @var string */
    private $event;

    /** @var array */
    private $payload;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $source, string $resource, string $event, array $payload)
    {
        $this->source   = $source;
        $this->resource = $resource;
        $this->event    = $event;
        $this->payload  = $payload;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(WebhookHandler $handler)
    {
        // TODO Use try-catch

        $handler->handle(
            $this->source,
            $this->resource,
            $this->event,
            $this->payload
        );
    }
}
